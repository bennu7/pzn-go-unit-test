package service

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"pzn-go-unit-test/test/entity"
	"pzn-go-unit-test/test/respository"
	"testing"
)

var categoryRepositry = &respository.CategoryRepositoryMock{Mock: mock.Mock{}}
var categoryService = CategoryService{Repository: categoryRepositry}

func TestCategoryService_GetNotFound(t *testing.T) {

	//	program mock
	categoryRepositry.Mock.On("FindById", "1").Return(nil)

	category, err := categoryService.Get("1")
	assert.Nil(t, category)
	assert.NotNil(t, err)
}

// * category jika sukses
func TestCategoryService_GetSuccess(t *testing.T) {
	category := entity.Category{
		"2",
		"Handphone",
	}

	//TODO kembalian harus bernilai category & bukan nil
	categoryRepositry.Mock.On("FindById", "100").Return(category)
	//categoryRepositry.Mock.On("FindById", "100").Return(nil)

	result, err := categoryService.Get("100")
	assert.Nil(t, err)
	assert.NotNil(t, result)
	assert.Equal(t, category.Id, result.Id)
	assert.Equal(t, category.Name, result.Name)
}
