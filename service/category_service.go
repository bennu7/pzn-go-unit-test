package service

import (
	"errors"
	"pzn-go-unit-test/test/entity"
	"pzn-go-unit-test/test/respository"
)

type CategoryService struct {
	Repository respository.CategoryRepository
}

func (service CategoryService) Get(id string) (*entity.Category, error) {
	category := service.Repository.FindById(id)
	if category == nil {
		//return category, errors.New("Category not found")
		return nil, errors.New("Category not found")
	} else {
		return category, nil
	}
}
