package helper

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"runtime"
	"testing"
)

//? menggagalkan unit test
// ** t.Error() mirip dgn t.Fail -> tetap di eksekusi hingga akhir kode begitu ada error
// ** t.Fatal() mirip dgn t.FailNow -> kode tidak akan berlanjut hingga akhir begitu ketemu error

func TestHelloWorld(t *testing.T) {
	result := HelloWorld("ibnu")
	if result != "Hii ibnu" {
		t.Error("result must be Hello ibnu")
		//t.FailNow()
		//panic("result is not Hello ibnu")
	}
	fmt.Println("TestHelloWorld Done")
}

func TestHelloWorldBennu(t *testing.T) {
	result := HelloWorld("bennu")
	if result != "Hello bennu" {
		t.Fail()
		//panic("result is not Hello bennu")
	}
	fmt.Println("TestHelloWorldBennu Done")
}

// ! 05 assertion and require using package github.com/stretchr/testify/
func TestHelloWorldAssertion(t *testing.T) {
	result := HelloWorld("ibnu")
	assert.Equal(t, "Hai ibnu", result, "result must be hii ibnu")
	fmt.Println("di eksekusi assert.equal")
}

func TestHelloWorldRequire(t *testing.T) {
	result := HelloWorld("ibnu")
	require.Equal(t, "Hi ibnu", result, "result must be hii ibnu")
	fmt.Println("di eksekusi require.equal")
}

// ! 06 Skip Test
func TestSkip(t *testing.T) {
	if runtime.GOOS == "darwin" {
		t.Skip("Unit test tidak bisa jalan di MAC")
	}

	result := HelloWorld("ibnu")
	require.Equal(t, "Hii ibnu", result)
}

// ! 07 Before and after Test
// ! this great function Before and After Test using *testing.M
func TestMain(m *testing.M) {
	//before
	fmt.Println("Sebelum unit Test")

	m.Run() // eksekusi semua unit test

	//after
	fmt.Println("Setelah Unit Test")
}

// ! 08 Sub Test
func TestSubTest(t *testing.T) {
	// * if u run all function this test, use `go test -v -run=TestSubTest`
	// * and if u run specific one test use `go test -v -run=TestSubTest/ibnu`
	t.Run("ibnu", func(t *testing.T) {
		result := HelloWorld("ibnu")
		require.Equal(t, "wkwk ibnu", result, "result mus be Hii ibnu")
	})

	t.Run("lalu", func(t *testing.T) {
		result := HelloWorld("lalu")
		require.Equal(t, "Hello lalu", result, "result mus be Hii lalu")
	})
}

// !09 Table Test
func TestHelloWorldTable(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
		msgError string
	}{
		{
			name:     "HelloWorld(ibnu)",
			request:  "ibnu",
			expected: "Hii ibnu",
			msgError: "must be Hii ibnu",
		},
		{
			name:     "HelloWorld(lalu)",
			request:  "lalu",
			expected: "Hai lalu",
			msgError: "must be Hii lalu",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := HelloWorld(test.request)
			assert.Equal(t, test.expected, result, test.msgError)
		})
	}
}
