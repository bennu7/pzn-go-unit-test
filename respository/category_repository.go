package respository

import "pzn-go-unit-test/test/entity"

type CategoryRepository interface {
	FindById(id string) *entity.Category
}
