package respository

import (
	"github.com/stretchr/testify/mock"
	"pzn-go-unit-test/test/entity"
)

type CategoryRepositoryMock struct {
	Mock mock.Mock
}

// TODO implementasi untuk CategoryRepositoryMock
func (repository *CategoryRepositoryMock) FindById(id string) *entity.Category {
	arguments := repository.Mock.Called(id)
	if arguments.Get(0) == nil {
		return nil
	}
	category := arguments.Get(0).(entity.Category)
	return &category
}
