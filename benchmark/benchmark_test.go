package benchmark

import (
	"pzn-go-unit-test/test/helper"
	"testing"
)

// *14 Table Benchmark
func BenchmarkHelloWorldTable(b *testing.B) {
	benchmarks := []struct {
		name    string
		request string
	}{
		{
			name:    "HelloWorld(ibnu)",
			request: "ibnu",
		},
		{
			name:    "HelloWorld(lalu ibnu)",
			request: "lalu ibnu",
		},
		{
			name:    "HelloWorld(lalu ibnu hidayatullah)",
			request: "lalu ibnu hidayatullah",
		},
	}

	for _, benchmark := range benchmarks {
		b.Run(benchmark.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				helper.HelloWorld(benchmark.name)
			}
		})
	}
}

// *13 Sub-Benchmark run terminal Sub Benchmark `go test -v -bench=BenchmarkSub/ibnu`
func BenchmarkSub(b *testing.B) {
	b.Run("ibnu", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			helper.HelloWorld("lalu ibnu hidayatullah")
		}
	})

	b.Run("lalu", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			helper.HelloWorld("lalu ibnu")
		}
	})
}

// *12 Membuat Benchmark running bench menggunakan perintah `go test -v -bench=BenchmarkHelloWorld`
// *12 if runnning bench without unit test, `go test -v -run=NotMathUnitTest -bench=BenchmarkHelloWorld`
func BenchmarkHelloWorld(b *testing.B) {
	for i := 0; i < b.N; i++ {
		helper.HelloWorld("lalu ibnu hidayatullah")
	}
}

func BenchmarkHelloWorldSecond(b *testing.B) {
	for i := 0; i < b.N; i++ {
		helper.HelloWorld("ibnu")
	}
}
